﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Data;

namespace DataAccess
{
    public class OrderModel
    {
        public long OrderID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public Nullable<decimal> OrderTotal { get; set; }

       
        public string ProductName { get; set; }
        public Nullable<decimal> ProductPrice { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public int OrderItemID { get; set; }
        public int ProductID { get; set; }
      
        public int Quantity { get; set; }
        public decimal SubTotal { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}