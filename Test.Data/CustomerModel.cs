﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Data
{
   public class CustomerModel
    {
        public int CustomerID { get; set; }
        public string CustomerFName { get; set; }
        public string CustomerLName { get; set; }
    }
}
