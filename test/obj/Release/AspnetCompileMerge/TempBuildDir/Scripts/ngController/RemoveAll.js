﻿var myApp = angular.module('RemoveApp', []);

myApp.controller('RemoveController', function ($scope, $http, $filter) {
    $scope.RemoveAll = function () {

        var r = confirm("Are you sure to remove remove all customers, orders, products and order items?");
        if (r == true) {
            $http({
                method: 'Post',
                url: '../Order/RemoveAll',
                dataType: "json"
            }).success(function (data) {
                alert("Removed all customers, orders, products and order items successfully.");
                console.log(data);

            })
        }
    }
});