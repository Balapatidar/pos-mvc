﻿var myApp = angular.module('OrderApp', []);

myApp.controller('OrderController', function ($scope, $http, $filter) {
    var orderBy = $filter('orderBy');
    $scope.GetProduct = function () {
        $http({
            method: 'Get',
            url: '../Order/ProductList'
        }).success(function (data) { $scope.ProdList = data; })
        //.error(function () { alert("error"); })
    }

    $scope.GetCustomer = function () {
        $http({
            method: 'Get',
            url: '../Order/CustomerList'
        }).success(function (data) { $scope.custList = data; })
        // .error(function () { alert("error");})
    }
    $scope.GetOrderList = function () {
        $http({
            method: 'Get',
            url: '../Order/OrderList1',
            dataType: "json"
        }).success(function (data) {
            $scope.orderLists = data;
            //console.log(data);
            //console.log(data.OrderDate);
            if (data.OrderDate != null) {
                var ddate = new Date(parseInt(data.OrderDate.substr(6)));// value.DueDate;
                var mth = ddate.getMonth() + 1;
                if (mth < 10) mth = "0" + mth;
                $scope.OrderDate = ddate.getDate() + "/" + mth + "/" + ddate.getFullYear();
            }
        })
        //.error(function () { alert("error"); })

    }
    $scope.orderSearchList = function (predicate, reverse) {
        $scope.orderLists = orderBy($scope.orderLists, predicate, reverse);
    };
    $scope.AddOrder = function () {
        // alert($("#txtQuan1").val())
        //  var SelCustomer = $("#custList").find(":selected").val();
        var selCustomer = $scope.custList1;
        if ((selCustomer == null) || (selCustomer == undefined)) {
            alert("Please Select Customer");
            return false;
        }
        var prod1 = $scope.ProdList1;
        var prod2 = $scope.ProdList2;
        var prod3 = $scope.ProdList3;
        var prod4 = $scope.ProdList4;
        // var quan1 = $scope.txtQuan1;
        var quan1 = $("#txtQuan1").val();
        if (quan1 > 0) {
            if ((prod1 == null) || (prod1 == undefined)) {
                alert("Please Select Product");
                return false;
            }
        }
        var quan2 = $("#txtQuan2").val();
        if (quan2 > 0) {
            if ((prod2 == null) || (prod2 == undefined)) {
                alert("Please Select Product");
                return false;
            }
        }
        var quan3 = $("#txtQuan3").val();
        if (quan3 > 0) {
            if ((prod3 == null) || (prod3 == undefined)) {
                alert("Please Select Product");
                return false;
            }
        }
        var quan4 = $("#txtQuan4").val();
        if (quan4 > 0) {
            if ((prod4 == null) || (prod4 == undefined)) {
                alert("Please Select Product");
                return false;
            }
        }
        $http({
            method: 'Get',
            params: { custId: selCustomer, ProdId1: prod1, Quan1: quan1, ProdId2: prod2, Quan2: quan2, ProdId3: prod3, Quan3: quan3, ProdId4: prod4, Quan4: quan4 },
            url: '../Order/InsertOrder'
        }).success(function (data) {
            $scope.result = data;
            alert("Order is successfully placed");
            $("#txtQuan1").val('');
            $("#txtQuan2").val('');
            $("#txtQuan3").val('');
            $("#txtQuan4").val('');
            $('#ProdList1').find('option:first').attr('selected', 'selected');
            $('#ProdList2').find('option:first').attr('selected', 'selected');
            $('#ProdList3').find('option:first').attr('selected', 'selected');
            $('#ProdList4').find('option:first').attr('selected', 'selected');
            $('#custList1').find('option:first').attr('selected', 'selected');
        })
       .error(function () { alert("error"); })
    }

    $scope.OrderSearch = function () {
      //  alert("SearchOrder")
        var cust = $("#txtCustName").val();
        var orderNum = $("#txtOrderNo").val();
        var orderTot = $("#txtOrderTotal").val();
        var product = $("#productList :selected").val()
        $http({
            method: 'Get',
            url: '../Order/SearchOrder',
            params: { cust: cust, orderNum: orderNum, orderTot: orderTot, product: product },
            dataType: "json"
        }).success(function (data) {
            $scope.orderLists = data;
            console.log(data);
            console.log(data.OrderDate);
            if (data.OrderDate != null) {
                var ddate = new Date(parseInt(data.OrderDate.substr(6)));// value.DueDate;
                var mth = ddate.getMonth() + 1;
                if (mth < 10) mth = "0" + mth;
                $scope.OrderDate = ddate.getDate() + "/" + mth + "/" + ddate.getFullYear();
            }
        })
    };
    $scope.ShowDetail = function (value, ordDate, Customer, ItemCount, OrderTotal) {
              
        document.getElementById("dvOrdNum").innerHTML = value;
        document.getElementById("dvCust").innerHTML = Customer;
        document.getElementById("dvOrdTot").innerHTML = OrderTotal;
        if (ordDate != null) {
            var ddate = new Date(parseInt(ordDate.substr(6)));// value.DueDate;
            var mth = ddate.getMonth() + 1;
            if (mth < 10) mth = "0" + mth;
            document.getElementById("dvOrddate").innerHTML = ddate.getDate() + "/" + mth + "/" + ddate.getFullYear();
        }
              
        $http({
            method: 'Post',
            url: '../Order/ShowOrderDetail',
            params: { orderNum: value },
            dataType: "json"
        }).success(function (data) {
            $scope.orderShows = data;
            console.log(data);
           
        })


       
    }
    $scope.AddCust = function () {
      
        var FName = $("#txtFName").val();
        var LName = $("#txtLName").val();
        $http({
            method: 'Post',
            url: '../Order/InsertCustomer',
            params: { FName: FName, LName: LName },
            dataType: "json"
        }).success(function (data) {
            alert("Customer is added successfully");
            $("#txtFName").val('');
            $("#txtLName").val('');
            console.log(data);

        })
    }
    $scope.AddProduct = function () {
        var PName = $("#txtPName").val();
        var PPrice = $("#txtPNamePrice").val();
        if (PName.trim() == "") { alert("Please enter product name"); $("#txtPName").focus(); return false; }
        if (PPrice.trim() == "") { alert("Please enter product price"); $("#txtPNamePrice").focus(); return false; }
        $http({
            method: 'Post',
            url: '../Order/InsertProduct',
            params: { PName: PName, PPrice: PPrice },
            dataType: "json"
        }).success(function (data) {
            alert("Product is added successfully");
            $("#txtPName").val('');
            $("#txtPNamePrice").val('');
            console.log(data);

        })
    }
    $scope.RemoveAll = function () {

        var r = confirm("Are you sure to remove remove all customers, orders, products and order items?");
        if (r == true) {
            $http({
                method: 'Post',
                url: '../Order/RemoveAll',
                dataType: "json"
            }).success(function (data) {
                alert("Removed all customers, orders, products and order items successfully.");
                console.log(data);

            })
        }
    }
    $scope.CreateDemo = function () {

        var r = confirm("Are you sure to Create New Demo all customers, orders, products and order items?");
        if(r == true)
        {
            var selCustomer = $scope.custList1;
            if ((selCustomer == null) || (selCustomer == undefined)) {
                alert("Please Select Customer");
                return false;
            }
            var prod1 = $scope.ProdList1;
               
           
            //var selCustomer = $scope.custList1;
            //alert(selCustomer);
            //if ((selCustomer == null) || (selCustomer == undefined)) {
            //    alert("Please Select Customer");
            //    return false;
            //}
            $http({
                method: 'Post',
                url: '../Order/createnewDemo',
                params: { custId: selCustomer, ProdId1: prod1 },
                dataType: "json"
            }).success(function (data) {
                alert("Create Demo customers, orders, products and order items successfully.");
                console.log(data);

            })

        }
    }
    $scope.GetExcelList1 = function () {
        $scope.Exportfile = $("#ExportDiv").html().toString();//.innerHTML;
        $http({
            method: 'Post',
            url: '../Order/GenrateExcel',
            data: { Expfile: $scope.Exportfile }
        }).success(function (data) {
            //$scope.custList = data;
        })
    };
  
});