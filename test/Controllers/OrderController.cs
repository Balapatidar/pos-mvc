﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test.Models;

namespace test.Controllers
{
    public class OrderController : Controller
    {
        SchTestEntities objTestEntites;
        //
        // GET: /Order/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult OrderList()
        {

            return View();
        }

        public string OrderList1()
        {           
            try
            {
                              
                DataAccess.OrderService objOrder=new DataAccess.OrderService();
                var ret = objOrder.OrderList();
                return Newtonsoft.Json.JsonConvert.SerializeObject(ret);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public string SearchOrder(string cust, string orderNum, string orderTot, string product)
        {
            DataAccess.OrderService objOrder = new DataAccess.OrderService();
            var ret = objOrder.SearchOrderList(cust, orderNum, orderTot,product);
            return Newtonsoft.Json.JsonConvert.SerializeObject(ret);  
        }
        public string ProductList()
        {
            string retrn = string.Empty;
            try
            {
                DataAccess.ProductService objProduct = new DataAccess.ProductService();
                var ret = objProduct.ProductList();
                return Newtonsoft.Json.JsonConvert.SerializeObject(ret);  
              //  return Json(ret, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ActionResult NewOrder()
        {

            return View();
        }

        public string ShowOrderDetail(string orderNum)
        {
          
             DataAccess.OrderService objOrder = new DataAccess.OrderService();
             var ret = objOrder.ShowOrderDetail(orderNum);

             return Newtonsoft.Json.JsonConvert.SerializeObject(ret);
           
        }
        public bool InsertOrder(int custId, string ProdId1, string Quan1, string ProdId2, string Quan2, string ProdId3, string Quan3, string ProdId4, string Quan4)
        {
           bool rt=false;
            DataAccess.OrderService objOrder = new DataAccess.OrderService();
            try{
                rt = objOrder.insertOrder(custId, ProdId1, Quan1, ProdId2, Quan2, ProdId3, Quan3, ProdId4, Quan4);
            }
            catch{
                rt=false;
            }
            return rt;
        }
        public JsonResult CustomerList()
        {

            string retrn = string.Empty;
            try
            {
                DataAccess.CustomerService objCust = new DataAccess.CustomerService();               
               var ret =  objCust.CustomerList();
               
                return Json(ret, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
               
                throw ex;
            }
            
        }

        public ActionResult NewCustomer()
        {
            return View();

        }

        public bool InsertCustomer( string FName, string LName)
        {
            bool rt = false;
            DataAccess.CustomerService objCustService = new DataAccess.CustomerService();    
            
            try
            {
                Test.Data.Customer objCust = new Test.Data.Customer();
                if (FName.Trim() != string.Empty)
                    {
                        objCust.CustomerFName = FName;
                        objCust.CustomerLName = LName;
                        objCustService.AddCustomer(objCust);
                }
                
                rt = true;
            }
            catch
            {
                rt = false;
            }
            return rt;
        }
        public ActionResult NewProduct()
        {
            return View();

        }

        public bool InsertProduct(string PName, string PPrice)
        {
            bool rt = false;
            DataAccess.clsDataFunction objDBAccess = new DataAccess.clsDataFunction();
            objTestEntites = new SchTestEntities();

            try
            {
                Test.Data.Product objProd = new Test.Data.Product();
                if (PName.Trim() != string.Empty)
                {
                    objProd.ProductName = PName;
                    objProd.ProductPrice = Convert.ToDecimal(PPrice);
                    objDBAccess.AddProduct(objProd);
                }

                rt = true;
            }
            catch
            {
                rt = false;
            }
            return rt;
        }

        public ActionResult RemoveAllData()
        {
            return View();

        }
        public bool RemoveAll()
        {
            bool rt = false;
             objTestEntites = new SchTestEntities();
             using (System.Data.Entity.DbContextTransaction dbTran = objTestEntites.Database.BeginTransaction())
             {
                 try
                 {                     
                     objTestEntites.Database.ExecuteSqlCommand("delete from OrderItems");
                     objTestEntites.Database.ExecuteSqlCommand("delete from Orders");
                     objTestEntites.Database.ExecuteSqlCommand("delete from Customers");
                     objTestEntites.Database.ExecuteSqlCommand("delete from Products");
                    
                     dbTran.Commit();
                     rt = true;
                 }
                 catch
                 {
                     dbTran.Rollback();
                     rt = false;
                 }
             }
             return rt;
        }

        public ActionResult CreateDemo()
        {
            return View();
        }

        public bool createnewDemo(int custId, string ProdId1)
        {
            bool rt = false;
            try
            {
                            
            objTestEntites = new SchTestEntities();
            DataAccess.clsDataFunction objDBAccess = new DataAccess.clsDataFunction();
            DataAccess.CustomerService objCustService = new DataAccess.CustomerService(); 
            Test.Data.Product objProd = new Test.Data.Product();
                Random r = new Random();
            objProd.ProductName = "Demo Product"+r.Next(5,21);                                            // Insert product
            objProd.ProductPrice = Convert.ToDecimal(100);
            objDBAccess.AddProduct(objProd);
               
            //var ProdDemo= objTestEntites.Products.FirstOrDefault();
            //int PID=  ProdDemo.ProductID;
            //Test.Data.Customer objCust = new Test.Data.Customer();                        // Insert Cusomer
            //objCust.CustomerFName = "Demo";
            //objCust.CustomerLName = "Test";
            //objCustService.AddCustomer(objCust);

            DataAccess.OrderService objOrder = new DataAccess.OrderService();               // Insert Product order
            //var prod1 = objTestEntites.Products.LastOrDefault();

            rt = objOrder.insertOrder(custId, ProdId1, "10", " ", " ", " ", " ", " ", " ");
            }
            catch
            {

                rt = false;
            }
            return rt;
        }

        public string GenrateExcel(string Expfile)
        {
            Session["html"] = Expfile;
            return Expfile;
        }
        [HttpPost]
        public ActionResult GenrateExcel1(FormCollection colle)
        {
            if (Session["html"] == null)
                Session["html"] = "";
            string col = Session["html"].ToString();
            Response.AppendHeader("content-disposition", "attachment;filename=ExportedHtml.xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application/vnd.ms-excel";
            //this.EnableViewState = false;
            Response.Write(col);
            Response.End();
            return RedirectToAction("OrderList");
        }

        [HttpPost]
        public ActionResult GenrateCSV(FormCollection colle)
        {
            if (Session["html"] == null)
                Session["html"] = "";
            string col = Session["html"].ToString();
            Response.AppendHeader("Content-Disposition", "attachment; filename=ExportedHtml.csv");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "text/csv";
            //this.EnableViewState = false;
            Response.Write(col);
            Response.End();
            return RedirectToAction("OrderList");
        }
	}
}
