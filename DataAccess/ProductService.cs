﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Data;

namespace DataAccess
{
    public interface IProductService
    {
        int AddProduct(Product product);
    }
    public class ProductService:IProductService
    {
        public int AddProduct(Product product)
        {
            return 0;
        }

        public List<ProductModel> ProductList()
        {
            SchTestEntities objTestEntites = new SchTestEntities();
            var ret = objTestEntites.Products.ToList();
            List<ProductModel> pd = new List<ProductModel>();
            foreach (var rt in ret)
            {
                Mapper.CreateMap<Product, ProductModel>();
                var md= Mapper.Map<Product, ProductModel>(rt);
                pd.Add(md);
            }
            return pd;
        }
    }
}
