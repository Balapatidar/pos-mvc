﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Data;

namespace DataAccess
{
    public class clsDataFunction
    {
        SchTestEntities objTestEntites;
        public bool AddOrder(Order order)
        {
            bool rt = false;
            try
            {
                objTestEntites = new SchTestEntities();
                objTestEntites.Orders.Add(order);
                objTestEntites.SaveChanges();
                rt = true;
            }
            catch (Exception ex)
            {
                rt = false;
                throw ex;
            }
            return rt;
        }
        public bool UpdateOrder(Order order)
        {
            bool rt = false;
            try
            {
                objTestEntites = new SchTestEntities();
                objTestEntites.Orders.Add(order);
                objTestEntites.SaveChanges();
                rt = true;
            }
            catch (Exception ex)
            {
                rt = false;
                throw ex;
            }
            return rt;
        }

        public bool AddOrderItem(OrderItem orderItem)
        {
            bool rt = false;
            try
            {
                objTestEntites = new SchTestEntities();
                objTestEntites.OrderItems.Add(orderItem);
                objTestEntites.SaveChanges();
                rt = true;
            }
            catch (Exception ex)
            {
                rt = false;
                throw ex;
            }
            return rt;
        }

        public Boolean AddCustomer(Customer cust)
        {
            bool rt = false;
            try
            {
                objTestEntites = new SchTestEntities();
                objTestEntites.Customers.Add(cust);
                objTestEntites.SaveChanges();
                rt = true;
            }
            catch (Exception ex)
            {
                rt = false;
                throw ex;
            }
            return rt;
        }

        public Boolean AddProduct(Product prod)
        {
            bool rt = false;
            try
            {
                objTestEntites = new SchTestEntities();
                objTestEntites.Products.Add(prod);
                objTestEntites.SaveChanges();
                rt = true;
            }
            catch (Exception ex)
            {
                rt = false;
                throw ex;
            }
            return rt;
        }

    }
}
