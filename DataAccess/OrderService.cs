﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Data;

namespace DataAccess
{
    public class OrderService
    {
        public List<OrderModel> OrderList()
        {
            SchTestEntities objTestEntites = new SchTestEntities();
            List<OrderModel> mdl = new List<OrderModel>();
            var ret = (from ep in objTestEntites.Orders
                       join e in objTestEntites.Customers on ep.CustomerID equals e.CustomerID
                       join t in objTestEntites.OrderItems on ep.OrderID equals t.OrderID
                       select new
                       {
                           OrderNumber = ep.OrderID,
                           OrderID = ep.OrderID,
                           OrderDate = ep.OrderDate,
                           Customer = e.CustomerFName + " " + e.CustomerLName,
                           CustomerFName = e.CustomerFName,
                           CustomerLName = e.CustomerLName,
                           ItemCount = t.Quantity,
                           OrderTotal = ep.OrderTotal,
                           ProductId = t.ProductID
                       }).Take(20);
            foreach (var rt in ret)
            {
                OrderModel d = new OrderModel();
                d.OrderID = rt.OrderID;
                d.OrderDate = rt.OrderDate;
                d.ProductID = rt.ProductId;
                d.CustomerName = rt.Customer;
                d.OrderTotal = rt.OrderTotal;
                d.Quantity = rt.ItemCount;
                mdl.Add(d);  
            }
            return mdl;
        }


        public List<OrderModel> SearchOrderList(string cust, string orderNum, string orderTot, string product)
        {
            string custName = string.Empty;
            int orderNo = 0;
            int orderTotal = 0;
            int prodId = 0;
            if (cust.Trim() != string.Empty) { custName = cust; }
            if (orderNum.Trim() != string.Empty) { orderNo = Convert.ToInt32(orderNum); }
            if (orderTot.Trim() != string.Empty) { orderTotal = Convert.ToInt32(orderTot); }
            if (product.Trim() != string.Empty) { prodId = Convert.ToInt32(product); }
            SchTestEntities objTestEntites = new SchTestEntities();
            List<OrderModel> mdl = new List<OrderModel>();
            var ret = (from ep in objTestEntites.Orders
                       join e in objTestEntites.Customers on ep.CustomerID equals e.CustomerID
                       join t in objTestEntites.OrderItems on ep.OrderID equals t.OrderID
                       select new
                       {
                           OrderNumber = ep.OrderID,
                           OrderID = ep.OrderID,
                           OrderDate = ep.OrderDate,
                           Customer = e.CustomerFName + " " + e.CustomerLName,
                           CustomerFName = e.CustomerFName,
                           CustomerLName = e.CustomerLName,
                           ItemCount = t.Quantity,
                           OrderTotal = ep.OrderTotal,
                           ProductId = t.ProductID
                       });

            if (cust.Trim() != string.Empty)
            {
                ret = ret.Where(e => e.Customer.Contains(custName));
            }
            if (orderNum.Trim() != string.Empty)
            {
                ret = ret.Where(ep => ep.OrderID == orderNo);
            }
            if (orderTot.Trim() != string.Empty)
            {
                ret = ret.Where(ep => ep.OrderTotal == orderTotal);
            }
            if (product.Trim() != string.Empty)
            {
                ret = ret.Where(p1 => p1.ProductId == prodId);
            }
            foreach (var rt in ret)
            {
                OrderModel d = new OrderModel();
                d.OrderID = rt.OrderID;
                d.ProductID = rt.ProductId;
                d.OrderDate = rt.OrderDate;
                d.CustomerName = rt.Customer;
                d.OrderTotal = rt.OrderTotal;
                d.Quantity = rt.ItemCount;
                mdl.Add(d);
            }
            return mdl;
        }

        public List<OrderModel> ShowOrderDetail(string orderNum)
        {
            SchTestEntities objTestEntites = new SchTestEntities();
            List<OrderModel> mdl = new List<OrderModel>();
            int orderNo = 0;
            if (orderNum.Trim() != string.Empty) { orderNo = Convert.ToInt32(orderNum); }
            string retrn = string.Empty;
            try
            {
                objTestEntites = new SchTestEntities();
                var ret = (from t in objTestEntites.OrderItems
                           join t1 in objTestEntites.Products on t.ProductID equals t1.ProductID
                           where t.OrderID == orderNo
                           select new
                           {
                               OrderID = t.OrderID,
                               ProductID = t1.ProductID,
                               ProductName = t1.ProductName,
                               ProductPrice = t1.ProductPrice,
                               Quantity = t.Quantity,
                               SubTotal = t1.ProductPrice * t.Quantity

                           });
               

                if (orderNum.Trim() != string.Empty)
                {
                    foreach (var rt in ret)
                    {
                        OrderModel d = new OrderModel();
                        d.OrderID = rt.OrderID;
                        d.ProductID = rt.ProductID;
                        d.ProductName = rt.ProductName;
                        d.ProductPrice = rt.ProductPrice;
                        d.Quantity = rt.Quantity;
                        d.SubTotal = Convert.ToDecimal(rt.SubTotal);
                        mdl.Add(d);
                    }
                }
                 }
            catch{                
            }
           
                return mdl;
            

        }

        public bool insertOrder(int custId, string ProdId1, string Quan1, string ProdId2, string Quan2, string ProdId3, string Quan3, string ProdId4, string Quan4)
        {
             bool rt = false;
            DataAccess.clsDataFunction objDBAccess = new DataAccess.clsDataFunction();
           SchTestEntities objTestEntites = new SchTestEntities();
            long ordId = 0;
            try
            {
                Order objorder = new Order();
                OrderItem objOrderItem = new OrderItem();
                decimal totOrder = 0;
                if (ProdId1 != null)
                {
                    if (ProdId1.Trim() != string.Empty)
                    {
                        int p1 = Convert.ToInt32(ProdId1.Trim());
                        var prod1 = objTestEntites.Products.Where(x => x.ProductID == p1).First();
                        decimal ordTot = Convert.ToDecimal(prod1.ProductPrice * Convert.ToInt32(Quan1.Trim()));
                        totOrder = ordTot;
                    }
                }
                if (ProdId2 != null)
                {
                    if (ProdId2.Trim() != string.Empty)
                    {
                        int p2 = Convert.ToInt32(ProdId2.Trim());
                        var prod1 = objTestEntites.Products.Where(x => x.ProductID == p2).First();
                        decimal ordTot = Convert.ToDecimal(prod1.ProductPrice * Convert.ToInt32(Quan2.Trim()));
                        totOrder += ordTot;
                    }
                }
                if (ProdId3 != null)
                {
                    if (ProdId3.Trim() != string.Empty)
                    {
                        int p3 = Convert.ToInt32(ProdId3.Trim());
                        var prod1 = objTestEntites.Products.Where(x => x.ProductID == p3).First();
                        decimal ordTot = Convert.ToDecimal(prod1.ProductPrice * Convert.ToInt32(Quan3.Trim()));
                        totOrder += ordTot;
                    }
                }
                if (ProdId4 != null)
                {
                    if (ProdId4.Trim() != string.Empty)
                    {
                        int p4 = Convert.ToInt32(ProdId4.Trim());
                        var prod1 = objTestEntites.Products.Where(x => x.ProductID == p4).First();
                        decimal ordTot = Convert.ToDecimal(prod1.ProductPrice * Convert.ToInt32(Quan3.Trim()));
                        totOrder += ordTot;
                    }
                }
                if (ProdId1 != null)
                {
                    if (ProdId1.Trim() != string.Empty)
                    {
                        int p1 = Convert.ToInt32(ProdId1.Trim());
                        var prod1 = objTestEntites.Products.Where(x => x.ProductID == p1).First();
                        decimal ordTot = Convert.ToDecimal(prod1.ProductPrice * Convert.ToInt32(Quan1.Trim()));
                        objorder.OrderDate = System.DateTime.Today;
                        objorder.OrderTotal = totOrder;
                        objorder.CustomerID = custId;
                        objDBAccess.AddOrder(objorder);
                        //insert into order item table
                        // var ordId = objTestEntites.Orders.Select(x => x.OrderID).Max();
                         ordId = objorder.OrderID;
                         objOrderItem.OrderID = ordId;
                         objOrderItem.ProductID = p1;
                         objOrderItem.Quantity = Convert.ToInt32(Quan1.Trim());
                         objDBAccess.AddOrderItem(objOrderItem);

                    }
                }
                if (ProdId2 != null)
                {
                    if (ProdId2.Trim() != string.Empty)
                    {
                        int p2 = Convert.ToInt32(ProdId2.Trim());                       
                        objOrderItem.OrderID = ordId;
                        objOrderItem.ProductID = p2;
                        objOrderItem.Quantity = Convert.ToInt32(Quan2.Trim());
                        objDBAccess.AddOrderItem(objOrderItem);
                    }
                }
                if (ProdId3 != null)
                {
                    if (ProdId3.Trim() != string.Empty)
                    {
                        int p3 = Convert.ToInt32(ProdId3.Trim());
                       
                        //insert into order item table
                       // long ordId = objorder.OrderID;
                        objOrderItem.OrderID = ordId;
                        objOrderItem.ProductID = p3;
                        objOrderItem.Quantity = Convert.ToInt32(Quan3.Trim());
                        objDBAccess.AddOrderItem(objOrderItem);
                    }
                }
                if (ProdId4 != null)
                {
                    if (ProdId4.Trim() != string.Empty)
                    {
                        int p4 = Convert.ToInt32(ProdId4.Trim());                       

                        //insert into order item table
                       // long ordId = objorder.OrderID;
                        objOrderItem.OrderID = ordId;
                        objOrderItem.ProductID = p4;
                        objOrderItem.Quantity = Convert.ToInt32(Quan4.Trim());
                        objDBAccess.AddOrderItem(objOrderItem);
                    }
                }
            }
            catch
            {
                rt = false;
            }
            return rt;

        }
    }
}
