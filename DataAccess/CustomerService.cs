﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Data;

namespace DataAccess
{
   public class CustomerService
    {
       public List<CustomerModel> CustomerList()
       {
           //SchTestEntities objTestEntites = new SchTestEntities();
           //var ret = objTestEntites.Customers.ToList();
           //return ret;
           SchTestEntities objTestEntites = new SchTestEntities();
           var ret = objTestEntites.Customers.ToList();
           List<CustomerModel> pd = new List<CustomerModel>();
           foreach (var rt in ret)
           {
               Mapper.CreateMap<Customer, CustomerModel>();
               var md = Mapper.Map<Customer, CustomerModel>(rt);
               pd.Add(md);
           }
           return pd;
       }

       public Boolean AddCustomer(Customer cust)
       {
           bool rt = false;
           try
           {
               SchTestEntities objTestEntites = new SchTestEntities();
               objTestEntites.Customers.Add(cust);
               objTestEntites.SaveChanges();
               rt = true;
           }
           catch (Exception ex)
           {
               rt = false;
               throw ex;
           }
           return rt;
       }
    }
}
